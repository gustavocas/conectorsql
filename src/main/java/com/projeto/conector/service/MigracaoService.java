package com.projeto.conector.service;

import com.projeto.conector.util.DataSource;
import com.projeto.conector.util.LerParametros;
import com.projeto.conector.vo.ConexaoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Date;

@Service
public class MigracaoService {

    @Autowired
    LerParametros lerParametros;

    public void migrar(String empresa1, String empresa2, String tabelaOrigem, String tabelaDestino) throws Exception {

        Connection con=null;
        Statement stmt=null;
        ResultSet rs=null;

        Connection con2=null;
        Statement stmt2=null;


        try {
            System.out.println("INICIO EXPORTACAO "+new Date());
            System.gc();
            Runtime rt = Runtime.getRuntime();
            long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
            System.out.println("Uso de memoria total" + usedMB);

            ConexaoVO conexaoOrigemVO = lerParametros.obterParametros(empresa1);
            ConexaoVO conexaoDestinoVO = lerParametros.obterParametros(empresa2);

            con = DataSource.getConexao(conexaoOrigemVO);
            stmt = con.createStatement();
            rs = stmt.executeQuery(tabelaOrigem);

            con2 = DataSource.getConexao(conexaoDestinoVO);
            stmt2 = con2.createStatement();


            ResultSetMetaData rsMetaDado = rs.getMetaData();
            StringBuilder linha = new StringBuilder();
            int cont=0;
            while(rs.next()) {
                linha = new StringBuilder();
                linha.append("INSERT INTO "+tabelaDestino+" VALUES(");
                for (int i = 1; i <= rsMetaDado.getColumnCount(); i++) {
                    if (i>1){
                        linha.append(",");
                    }
                    if (rs.getObject(i)==null) {
                        linha.append("null");
                    }else{
                        linha.append("'" + rs.getObject(i) + "'");
                    }
                }
                linha.append(")");
                stmt2.execute(linha.toString());

                cont++;
                if (cont%1000==0){
                    System.out.println("Linha: "+cont);
                }
            }


            System.out.println("INICIO FIM "+new Date());
            rt = Runtime.getRuntime();
            long usedMB2 = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
            System.out.println("Uso de memoria total" + usedMB);
            System.out.println("Uso de memoria processo" + (usedMB2 - usedMB));
        }catch(Exception e) {
            e.printStackTrace();
            throw e;
        }finally {
            if(rs!=null) rs.close();
            stmt.close();
            con.close();
        }

    }
}
