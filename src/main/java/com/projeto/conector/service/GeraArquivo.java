package com.projeto.conector.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Date;

import com.projeto.conector.util.LerParametros;
import com.projeto.conector.vo.ConexaoVO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.conector.util.DataSource;

@Service
public class GeraArquivo {

	@Autowired
	LerParametros lerParametros;

	public void gerarArquivo(Writer writer,String empresa, String tabela, Integer filtro) throws Exception {
		
		Connection con=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try {
			System.out.println("INICIO EXPORTACAO "+new Date());
			System.gc();
		    Runtime rt = Runtime.getRuntime();
		    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
		    System.out.println("Uso de memoria total" + usedMB);

		    ConexaoVO conexaoVO = lerParametros.obterParametros(empresa);
		    
			con = DataSource.getConexao(conexaoVO);
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from "+tabela);
			
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
			
			ResultSetMetaData rsMetaDado = rs.getMetaData();
			StringBuilder linha = new StringBuilder();
			for (int i=1;i<=rsMetaDado.getColumnCount();i++) {
				csvPrinter.print(rsMetaDado.getColumnName(i));
			}

			csvPrinter.println();
			csvPrinter.printRecords(rs);
			
			csvPrinter.close();
		
			System.out.println("INICIO FIM "+new Date());
			rt = Runtime.getRuntime();
			long usedMB2 = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
		    System.out.println("Uso de memoria total" + usedMB);
			System.out.println("Uso de memoria processo" + (usedMB2 - usedMB));
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			if(rs!=null) rs.close();
			stmt.close();
			con.close();
		}
		
	}
	
public String gerarArquivoRes(String empresa, String tabela, Integer filtro) throws Exception {
		
		Connection con=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try {
			System.out.println("INICIO EXPORTACAO "+new Date());
			System.gc();
		    Runtime rt = Runtime.getRuntime();
		    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
		    System.out.println("memory usage" + usedMB);

			ConexaoVO conexaoVO = lerParametros.obterParametros(empresa);
		    
			con = DataSource.getConexao(conexaoVO);
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from "+tabela);
			
			ResultSetMetaData rsMetaDado = rs.getMetaData();
			int qtdClounas = rsMetaDado.getColumnCount();
			
			String nomeArquivo=System.getProperty("user.home")+System.getProperty("file.separator")+tabela+".csv";
			File file = new File(nomeArquivo);
			FileWriter fileWriter = new FileWriter(file, true);
			
			for (int i=1;i<=qtdClounas;i++) {
				if (i>1) {
					fileWriter.write(";");
				}
				fileWriter.write(rsMetaDado.getColumnName(i));
			}
			
			while(rs.next()) {
				fileWriter.write("\r\n");
				for (int i=1;i<=qtdClounas;i++) {
					if (i>1) {
						fileWriter.write(";");
					}
					fileWriter.write((rs.getString(i)==null?"":rs.getString(i)));
				}				
			}
			fileWriter.close();
			
			System.out.println("INICIO FIM "+new Date());
			rt = Runtime.getRuntime();
		    usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
		    System.out.println("memory usage" + usedMB);
		    return nomeArquivo;
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			if(rs!=null) rs.close();
			stmt.close();
			con.close();
		}
		
	}

}
