package com.projeto.conector.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projeto.conector.vo.ConexaoVO;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

@Service
public class LerParametros {
    public ConexaoVO obterParametros(String empresa) throws Exception{

        URL url = new URL("https://bitbucket.org/gustavocas/conectorsql/raw/master/src/main/java/com/projeto/conector/util/configuracaoConectorSQL.txt");
        InputStream is = url.openStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String linha = br.readLine();
        ConexaoVO conexaoVO = new ConexaoVO();
        while (linha != null) {
            if(linha.startsWith(empresa)) {
                conexaoVO = decoder(linha.split("\\|")[1]);
            }
            linha = br.readLine();
        }
        br.close();
        return conexaoVO;
    }


    /*
    * Gerar o token em https://jwt.io/ com a estrutura:
    * {
    * "driver":"com.microsoft.sqlserver.jdbc.SQLServerDriver",
    * "url":"jdbc:sqlserver://127.0.0.1:1422;databaseName=AdventureWorks;user=usuario;password=senha;",
    * "usuario":"usuario",
    * "senha":"senha"
    * "ativo":"S"
    * }
    * */
    public ConexaoVO decoder(String jwtToken) throws Exception{
        java.util.Base64.Decoder decoder = java.util.Base64.getUrlDecoder();
        String[] parts = jwtToken.split("\\."); // split out the "parts" (header, payload and signature)

        String headerJson = new String(decoder.decode(parts[0]));
        String payloadJson = new String(decoder.decode(parts[1]));
        String signatureJson = new String(decoder.decode(parts[2]));

        ObjectMapper mapper = new ObjectMapper();
        ConexaoVO conexaoVO = mapper.readValue(payloadJson, ConexaoVO.class);
        return  conexaoVO;
    }
}
