package com.projeto.conector.resource;

import com.projeto.conector.service.MigracaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value="/migrar")
public class MigrarResource {

    @Autowired
    MigracaoService migrarService;

    @GetMapping(value="/{empresa1}/{empresa2}/{tabelaOrigem}/{tabelaDestino}")
    public ResponseEntity<String> migrarTabela(
            @PathVariable String empresa1,
            @PathVariable String empresa2,
            @PathVariable String tabelaOrigem,
            @PathVariable String tabelaDestino,
            HttpServletResponse servletResponse){
        try {
            migrarService.migrar(empresa1,empresa2,tabelaOrigem,tabelaDestino);
            return ResponseEntity.ok()
                    .body("Tabela migrada");
        }catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body(e.getMessage());
        }

    }

}
