package com.projeto.conector.resource;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projeto.conector.util.LerParametros;
import com.projeto.conector.vo.ConexaoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.conector.service.GeraArquivo;

@RestController
@RequestMapping(value="/extrair")
public class ExtrairDados {
	
	@Autowired
	GeraArquivo geraArquivo;

	@Autowired
	LerParametros lerParametros;

	@GetMapping(value="/{empresa}/{tabela}/{filtro}")
	public void gerarDadosCSV(
			@PathVariable String empresa,
			@PathVariable String tabela,
			@PathVariable Integer filtro,
			HttpServletResponse servletResponse){
		try {
			servletResponse.setContentType("text/csv");
	        servletResponse.addHeader("Content-Disposition","attachment; filename=\""+tabela+".csv\"");
	        geraArquivo.gerarArquivo(servletResponse.getWriter(),empresa,tabela, filtro);
        }catch(Exception e) {
        	try {
        		servletResponse.setStatus(400);
				servletResponse.getWriter().append(e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
	
	}
    
	@GetMapping(value="/arquivo/{tabela}")
    public ResponseEntity<String> gerarDadosArquivo(@PathVariable String tabela){
		try {
			String arq = geraArquivo.gerarArquivoRes("", tabela, new Integer(0));
	    	
	    	
			return ResponseEntity.ok()
			        .body("Arquivo gerado em "+arq);
		}catch(Exception e) {
			e.printStackTrace();
    		return ResponseEntity.ok().body(e.getMessage());			
        }
		
	}

	@GetMapping(value="/decode/{jwtToken}")
	public ResponseEntity<String> decodificarJWT(@PathVariable String jwtToken){
		try {

			ConexaoVO conexaoVO = lerParametros.decoder(jwtToken);

			return ResponseEntity.ok()
					.body("Resposta "
							+"<br>driver: "+conexaoVO.driver
							+"<br>url: " + conexaoVO.url
							+"<br>usuario: " + conexaoVO.usuario
							+"<br>senha: " + conexaoVO.senha
							+"<br>ativo: " + conexaoVO.ativo);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok().body(e.getMessage());
		}

	}
	
}
