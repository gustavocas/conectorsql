package com.projeto.conector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConectorSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConectorSqlApplication.class, args);
	}

}
